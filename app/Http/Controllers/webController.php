<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use App\Imports\uraianImport;
// use Excel;
use Illuminate\Support\Facades\Hash;
use App\tbl_buku;
use App\tbl_index;
use App\tbl_pinjam;
use App\tbl_rak;
use App\tbl_pengunjung;
use App\User;
use Yajra\DataTables\Datatables;
use Auth;

class webController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $now = date('Y-m-d');
        // dd($ip);
        $cek = DB::table('tbl_pengunjung')
            ->where('tgl_dtng',$now)
            ->where('ip',$ip)
            ->first();
        // dd($cek);
        if($cek == null){
            $pengunjung = new tbl_pengunjung;
            $pengunjung->id = Str::upper(Str::uuid());
            $pengunjung->tgl_dtng = $now;
            $pengunjung->ip = $ip;
            $pengunjung->timestamps = false;
    
            $pengunjung->save();
        }

        

        return view('welcome');
    }
    
    public function indexBuku()
    {
        $buku = DB::table('tbl_buku')
            ->get();
        return view('buku.index', compact('buku'));
    }

    public function dataBuku()
    {
        // $query = DB::table('tbl_buku')
        //     ->get();
        $query = tbl_buku::all();
        // dd($query);
        return DataTables::of($query)
            ->addIndexColumn()
            
            ->addColumn('judul_buku', function ($judul) {
                $button = '<a href="'.route('indexArsip',$judul->id_buku).'"><b>'.$judul->judul_buku.'</b></a>';
                return $button;
            })
            ->addColumn('action', function ($action) {
                $button = '<div class="d-flex">';
                $button .= '<button class="btn btn-sm btn-warning" onclick="editForm(\''.$action->id_buku.'\')">Edit</button>&nbsp;&nbsp;';
                $button .= ' <button class="btn btn-sm btn-danger" onclick="deleteData(\''.$action->id_buku.'\')">Hapus</button>';
                $button .= '</div>';
                return $button;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function indexArsip($id_buku)
    {
        if(Auth::user() == null){
            return redirect('login');
        }
        $buku = DB::table('tbl_buku')
            ->where('id_buku',$id_buku)
            ->first();
        $index = DB::table('tbl_index')
            ->where('id_buku',$id_buku)
            ->orderBy('no_index','ASC')
            ->get();
        $rak = DB::table('tbl_rak')
            ->get();
        $no = 1;
        // $buku = $id_buku;
        return view('index.index', compact('index','no','rak','buku'));
    }
    
    public function createUser(Request $request)
    {
        $user = new User;
        $user->id = Str::upper(Str::uuid());
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->password = Hash::make($request['password']);
        $user->role = 'User';
        $user->no_hp = $request['no_hp'];
        $user->alamat = $request['alamat'];
    
        $user->save();

        return redirect()->back()->with('success', 'Akun Berhasil Di Daftar, Silahkan Login');
    }
}
