<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_index extends Model
{
    protected $table = 'tbl_index';
    public $timestamps = false;
    protected $fillable = ['id_index','id_buku','no_index','judul','lampiran','fas_sub','pokok_masalah','sub_pokok_masalah','status'];
}
