<?php

namespace App\Imports;

use App\tbl_index;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;

// class uraianImport implements ToModel
class uraianImport implements ToModel, SkipsEmptyRows, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    use Importable;

    public function  __construct($id_buku)
    {
        $this->id_buku= $id_buku;
    }

    public function model(array $row)
    {
        // dd($row);
        $data = new tbl_index([
            'id_index' => Str::upper(Str::uuid()),
            'id_buku' => $this->id_buku,
            'no_index' => $row['no_reg'],
            'judul' => $row['masalah'],
            'lampiran' => $row['lampiran'],
            'fas_sub' => $row['fasilitatifsubtantif'],
            'pokok_masalah' => $row['pokok_masalah'],
            'sub_pokok_masalah' => $row['sub_pokok_masalah'],
            'status' => '1',
            // 'timestamps'=> false,
        ]);
        // dd($data);
        return $data;
    }

    public function headingRow(): int
    {
        return 6;
    }
}
