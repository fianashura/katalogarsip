<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']],
function(){ 
    Route::prefix('peminjaman')->group(function() {
        Route::get('/', 'PeminjamanController@pinjam')->name('pinjam');
        Route::get('/simpan', 'PeminjamanController@pinjam_simpan')->name('pinjam_simpan');
    });
    Route::prefix('kunjungan')->group(function() {
        Route::get('/', 'PeminjamanController@kunjungan')->name('kunjungan');
    });
    Route::prefix('listuser')->group(function() {
        Route::get('/', 'PeminjamanController@user')->name('user');
    });
});