<!-- sample modal content -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Input Peminjaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" action="{{route('pinjam_simpan')}}">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-body">
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Judul Buku</label>
                        <div class="col-9">
                            <select name="id_index" class="form-control" id="id_index">
                                <option selected disabled>Pilih Buku</option>
                                @foreach ($index as $in)
                                    <option value="{{$in->id_index}}">{{$in->judul}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Nama</label>
                        <div class="col-9">
                            <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Peminjam">
                        </div>
                    </div>
                    
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Alamat</label>
                        <div class="col-9">
                            <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat Peminjam">
                        </div>
                    </div>
                    
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">No. HP</label>
                        <div class="col-9">
                            <input type="text" name="no_hp" class="form-control" id="no_hp" placeholder="Nomer Hp Peminjam">
                        </div>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->