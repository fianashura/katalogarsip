<?php

namespace Modules\Peminjaman\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\tbl_buku;
use App\tbl_index;
use App\tbl_pinjam;
use App\tbl_rak;

class PeminjamanController extends Controller
{
    public function pinjam()
    {
        $index = DB::table('tbl_index')
            ->where('status','1')
            ->get();
        $pinjam = DB::table('tbl_pinjam')
            ->leftjoin('tbl_index','tbl_pinjam.id_index','tbl_index.id_index')
            ->get();
        $no = 1;
        return view('peminjaman::index', compact('index','pinjam','no'));
    }
    public function pinjam_simpan(Request $request)
    {
        $pinjam = new tbl_pinjam;
        $pinjam->id_pinjam = Str::upper(Str::uuid());
        $pinjam->id_index = $request['id_index'];
        $pinjam->nama = $request['nama'];
        $pinjam->alamat = $request['alamat'];
        $pinjam->no_hp = $request['no_hp'];
        $pinjam->tgl_pinjam = date('Y-m-d');
    
        $pinjam->timestamps = false;
        $pinjam->save();

        $index = DB::table('tbl_index')->where('id_index',$pinjam->id_index)->update([
            'status' => '0',
        ]);
        return back();
    }
    
    public function kunjungan()
    {
        $kunjungan = DB::table('tbl_pengunjung')
            ->select('tgl_dtng')
            ->groupBy('tgl_dtng')
            ->get();
        $no = 1;
        return view('peminjaman::kunjungan', compact('kunjungan','no'));
    }
    
    public function user()
    {
        $user = DB::table('users')
            ->where('role','User')
            ->get();
        $no = 1;
        return view('peminjaman::user', compact('user','no'));
    }
}
