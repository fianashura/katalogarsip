<?php

namespace Modules\Pengembalian\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\tbl_buku;
use App\tbl_index;
use App\tbl_pinjam;
use App\tbl_rak;

class PengembalianController extends Controller
{
    public function kembali()
    {
        $index = DB::table('tbl_index')
            ->where('status','0')
            // ->orderBy('tgl_pinjam','DESC')
            ->get();
        $pinjam = DB::table('tbl_pinjam')
            ->leftjoin('tbl_index','tbl_pinjam.id_index','tbl_index.id_index')
            ->orderBy('tbl_pinjam.tgl_pinjam','DESC')
            ->get();
        $no = 1;
        return view('pengembalian::index', compact('index','pinjam','no'));
    }
    public function kembali_simpan(Request $request, $id_pinjam)
    {       
        $index = DB::table('tbl_pinjam')->where('id_pinjam',$id_pinjam)->update([
            'tgl_kembali' => date('Y-m-d'),
        ]);
        
        $index = DB::table('tbl_index')->where('id_index',$request->id_index)->update([
            'status' => '1',
        ]);
        return back();
    }
}
