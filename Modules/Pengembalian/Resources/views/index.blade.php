@extends('home::layouts.master')

@section('title')
    Arsip | Daftar Peminjaman
@endsection

@section('css')
  <!-- plugin css -->
  <link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />   
@endsection

@section('content')
<div class="content">
                    
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Daftar Peminjaman</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">Daftar Peminjaman</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-1">Daftar Peminjaman
                    </h4>
                        <table id="basic-datatable" class="table dt-responsive" style="width:100%">
                            <thead>
                                <tr class="text-center">
                                    <th style="width:10%">#</th>
                                    <th style="width:80%">Masalah</th>
                                    <th style="width:80%">Nama</th>
                                    <th style="width:80%">Alamat</th>
                                    <th style="width:80%">No. HP</th>
                                    <th style="width:80%">Tgl Pinjam</th>
                                    <th style="width:80%">Status</th>
                                    <th style="width:5%"></th>
                                    <th style="width:5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pinjam as $bu)    
                                    <tr>
                                        <td>{{$no++}}.</td>
                                        <td><b>{{$bu->judul}}</b></td>
                                        <td>{{$bu->nama}}</td>
                                        <td>{{$bu->alamat}}</td>
                                        <td>{{$bu->no_hp}}</td>
                                        <td>{{$bu->tgl_pinjam}}</td>
                                        <td class="text-center">
                                            @if ($bu->tgl_kembali != NULL)
                                                <span class="badge badge-success">Telah Kembali</span>
                                            @else
                                                <span class="badge badge-danger">Belum Kembali</span>
                                            @endif
                                        </td>
                                        <td>
                                            <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#kembali-{{$bu->id_pinjam}}"><i data-feather="edit"></i></button>
                                            <!-- sample modal content -->
                                            <div id="kembali-{{$bu->id_pinjam}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="myModalLabel">Input Pengembalian</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form class="form-horizontal" action="{{route('kembali_simpan',$bu->id_pinjam)}}">
                                                            {{ csrf_field() }} {{ method_field('POST') }}
                                                            <div class="modal-body">
                                                                <div class="form-group row mb-3">
                                                                    <label for="inputEmail3" class="col-3 col-form-label">Judul Buku</label>
                                                                    <div class="col-9">
                                                                        <input type="hidden" name="id_index" class="form-control" id="id_index" value="{{$bu->id_index}}">
                                                                        <textarea name="index" class="form-control" id="index" style="height: max-content" disabled>{{$bu->judul}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label for="inputEmail3" class="col-3 col-form-label">Nama</label>
                                                                    <div class="col-9">
                                                                        <input type="hidden" name="nama" class="form-control" id="nama" value="{{$bu->nama}}">
                                                                        <input type="text" name="nama" class="form-control" id="nama" value="{{$bu->nama}}" disabled>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="form-group row mb-3">
                                                                    <label for="inputEmail3" class="col-3 col-form-label">Alamat</label>
                                                                    <div class="col-9">
                                                                        <input type="hidden" name="alamat" class="form-control" id="alamat" value="{{$bu->alamat}}">
                                                                        <input type="text" name="alamat" class="form-control" id="alamat" value="{{$bu->alamat}}" disabled>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="form-group row mb-3">
                                                                    <label for="inputEmail3" class="col-3 col-form-label">No. HP</label>
                                                                    <div class="col-9">
                                                                        <input type="hidden" name="no_hp" class="form-control" id="no_hp" value="{{$bu->no_hp}}">
                                                                        <input type="text" name="no_hp" class="form-control" id="no_hp" value="{{$bu->no_hp}}" disabled>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="form-group row mb-3">
                                                                    <label for="inputEmail3" class="col-3 col-form-label">Tanggal Kembali</label>
                                                                    <div class="col-9">
                                                                        <input type="hidden" name="tgl_kembali" class="form-control" id="tgl_kembali" value="{{date('Y-m-d')}}">
                                                                        <input type="date" name="tgl_kembali" class="form-control" id="tgl_kembali" value="{{date('Y-m-d')}}" disabled>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                                                                <button type="submit" class="btn btn-primary">Kembalikan</button>
                                                            </div>
                                                        </form>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-sm"><i data-feather="trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container-fluid -->
    {{-- @include('pengembalian::form') --}}
</div> <!-- content -->
@endsection

@section('js')

    <!-- datatable js -->
    <script src="{{asset('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>

    <!-- Datatables init -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
@endsection