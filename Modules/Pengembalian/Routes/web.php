<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']],
function(){ 
    Route::prefix('pengembalian')->group(function() {
        Route::get('/', 'PengembalianController@kembali')->name('kembali');
        Route::get('/simpan/{id_pinjam}', 'PengembalianController@kembali_simpan')->name('kembali_simpan');
    });
});
