<?php

namespace Modules\Rak\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\tbl_buku;
use App\tbl_index;
use App\tbl_pinjam;
use App\tbl_rak;

class RakController extends Controller
{
    public function rak()
    {
        $rak = DB::table('tbl_rak')
            ->get();
        $no = 1;
        return view('rak::index', compact('rak','no'));
    }
    public function rak_simpan(Request $request)
    {
        $rak = new tbl_rak;
        $rak->id_rak = Str::upper(Str::uuid());
        $rak->kode_rak = $request['kode_rak'];
    
        $rak->timestamps = false;
        $rak->save();

        return back();
    }
}
