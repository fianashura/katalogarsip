<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']],
function(){ 
    Route::prefix('rak')->group(function() {
        Route::get('/', 'RakController@rak')->name('rak');
        Route::get('/simpan', 'RakController@rak_simpan')->name('rak_simpan');
    });
});
