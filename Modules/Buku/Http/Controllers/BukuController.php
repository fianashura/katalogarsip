<?php

namespace Modules\Buku\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use App\Imports\uraianImport;
// use Excel;
use App\tbl_buku;
use App\tbl_index;
use App\tbl_pinjam;
use App\tbl_rak;
use Yajra\DataTables\Datatables;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function buku()
    {
        $buku = DB::table('tbl_buku')
            ->get();
        $no = 1;
        return view('buku::index', compact('buku','no'));
    }
    public function buku_simpan(Request $request)
    {
        $buku = new tbl_buku;
        $buku->id_buku = Str::upper(Str::uuid());
        $buku->judul_buku = $request['judul_buku'];
    
        $buku->timestamps = false;
        $buku->save();

        return response()->json([
            'success' => true,
            'title' => 'Sukses!',
            'tipe' => 'success',
            'message' => 'Data Buku Berhasil Terinput'
        ]);
    }
    
    public function update_buku(Request $request, $id)
    {
        // $buku = new tbl_buku;
        // $buku->id_buku = Str::upper(Str::uuid());
        // $buku->judul_buku = $request['judul_buku'];
    
        // $buku->timestamps = false;
        // $buku->save();
        DB::table('tbl_buku')->where('id_buku',$id)->update([
            'judul_buku' => $request->judul_buku
        ]);

        
        return response()->json([
            'success' => true,
            'title' => 'Sukses!',
            'tipe' => 'success',
            'message' => 'Data Buku Berhasil Terinput'
        ]);
    }
    
    public function hapus_buku($id)
    {
        // $buku = new tbl_buku;
        // $buku->id_buku = Str::upper(Str::uuid());
        // $buku->judul_buku = $request['judul_buku'];
    
        // $buku->timestamps = false;
        // $buku->save();
        DB::table('tbl_buku')->where('id_buku',$id)->delete();

        
        return response()->json([
            'success' => true,
            'title' => 'Sukses!',
            'tipe' => 'success',
            'message' => 'Data Buku Berhasil Dihapus'
        ]);
    }

    public function edit_buku($id)
    {
        $buku = tbl_buku::where('id_buku',$id)->first();
        return $buku;
    }

    public function dataBuku()
    {
        // $query = DB::table('tbl_buku')
        //     ->get();
        $query = tbl_buku::all();
        // dd($query);
        return DataTables::of($query)
            ->addIndexColumn()
            
            ->addColumn('judul_buku', function ($judul) {
                $button = '<a href="'.route('index_buku',$judul->id_buku).'"><b>'.$judul->judul_buku.'</b></a>';
                return $button;
            })
            ->addColumn('total_arsip', function ($total_arsip) {
                $total = DB::table('tbl_index')->where('id_buku',$total_arsip->id_buku)->count();
                // $button = '<a href="'.route('index_buku',$judul->id_buku).'"><b>'.$judul->judul_buku.'</b></a>';
                return $total.' Arsip';
            })
            ->addColumn('action', function ($action) {
                $button = '<div class="d-flex">';
                $button .= '<button class="btn btn-sm btn-warning" onclick="editForm(\''.$action->id_buku.'\')">Edit</button>&nbsp;&nbsp;';
                $button .= ' <button class="btn btn-sm btn-danger" onclick="deleteData(\''.$action->id_buku.'\')">Hapus</button>';
                $button .= '</div>';
                return $button;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function index($id_buku)
    {
        $buku = DB::table('tbl_buku')
            ->where('id_buku',$id_buku)
            ->first();
        $index = DB::table('tbl_index')
            ->where('id_buku',$id_buku)
            ->orderBy('no_index','ASC')
            ->get();
        $rak = DB::table('tbl_rak')
            ->get();
        $no = 1;
        // $buku = $id_buku;
        return view('buku::index.index', compact('index','no','rak','buku'));
    }

    public function index_simpan(Request $request, $id_buku)
    {
        $index = new tbl_index;
        $index->id_index = Str::upper(Str::uuid());
        $index->id_buku = $id_buku;
        $index->id_rak = $request['kode_rak'];
        $index->no_index = $request['no_index'];
        $index->judul = $request['judul'];
    
        $index->timestamps = false;
        $index->save();

        return back();
    }
    
    public function index_import(Request $request, $id_buku)
    {
		// menangkap file excel
		$file = $request->file('file');
        // dd($file);
		// membuat nama file unik
		$nama_file = rand().'-'.$file->getClientOriginalName();
        
        // dd($nama_file);

		// upload ke folder file_siswa di dalam folder public
		$file->move('file/arsip/',$nama_file);
        
		// import data
		// $excel = Excel::toArray([], public_path('/file/arsip/'.$nama_file));
        // Excel::import(new uraianImport,request()->file('file'));
        Excel::import(new uraianImport($id_buku) ,'file/arsip/'.$nama_file);
        // dd($excel); 
		// notifikasi dengan session
		// Session::flash('sukses','Data Siswa Berhasil Diimport!');
 
		// alihkan halaman kembali

        // Excel::load($request->file('file')->getRealPath(), function ($reader){
        //     foreach ($reader->toArray() as $key => $row) {
        //         dd($reader);
        //         // $data['nama_barang'] = (isset($row['nama_barang']) && $row['nama_barang']!='') ? $row['nama_barang'] : null;
        //         // $data['jumlah_barang'] = (isset($row['jumlah_barang']) && $row['jumlah_barang']!='') ? $row['jumlah_barang'] : null;
        //         // $data['keterangan'] = (isset($row['keterangan']) && $row['keterangan']!='') ? $row['keterangan'] : null;
        //         // $data['updated_by'] = Auth::user()->id;
        //         model::create($data);
        //     }
        // });

        // return redirect('barang');

        return back();
    }
    
    public function info()
    {
        return phpinfo();
    }

}
