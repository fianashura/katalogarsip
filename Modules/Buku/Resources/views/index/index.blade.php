@extends('home::layouts.master')

@section('title')
    Arsip | Masalah
@endsection

@section('css')
  <!-- plugin css -->
  <link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />   
@endsection

@section('content')
<div class="content">
                    
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Daftar Masalah</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{$buku->judul_buku}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-1">Daftar Masalah
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i data-feather="plus"></i></button>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#import"><i data-feather="upload"></i></button>
                    </h4>
                        <table id="basic-datatable" class="table dt-responsive" style="width:100%">
                            <thead>
                                <tr class="text-center">
                                    <th style="width:10%">#</th>
                                    <th style="width:10%">No. Registrasi</th>
                                    {{-- <th style="width:10%">Rak</th> --}}
                                    <th style="width:60%">Masalah</th>
                                    <th style="width:10%">Status</th>
                                    <th style="width:5%"></th>
                                    <th style="width:5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($index as $bu)    
                                    <tr>
                                        <td>{{$no++}}.</td>
                                        <td>{{$bu->no_index}}</td>
                                        {{-- <td>{{$bu->kode_rak}}</td> --}}
                                        <td>{{$bu->judul}}</td>
                                        <td class="text-center">
                                            @if ($bu->status == '1')
                                                <span class="badge badge-success">Tersedia</span>
                                            @else
                                                <span class="badge badge-danger">Keluar</span>
                                            @endif
                                        </td>
                                        <td>
                                            <button class="btn btn-warning btn-sm"><i data-feather="edit"></i></button>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-sm"><i data-feather="trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container-fluid -->
    @include('buku::index.form')
</div> <!-- content -->
@endsection

@section('js')

    <!-- datatable js -->
    <script src="{{asset('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>

    <!-- Datatables init -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
@endsection