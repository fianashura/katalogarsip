<!-- sample modal content -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Input Uraian Singkat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" action="{{route('index_simpan',$buku->id_buku)}}" method="POST">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-body">
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Kode Rak</label>
                        <div class="col-9">
                            <select name="kode_rak" class="form-control" id="kode_rak">
                                <option selected disabled>Pilih Rak</option>
                                @foreach ($rak as $ra)
                                    <option value="{{$ra->id_rak}}">{{$ra->kode_rak}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Index Buku</label>
                        <div class="col-9">
                            <input type="text" name="no_index" class="form-control" id="no_index" placeholder="No Index Buku">
                        </div>
                    </div>
                    
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Judul Buku</label>
                        <div class="col-9">
                            <input type="text" name="judul" class="form-control" id="judul" placeholder="Judul Buku">
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- sample modal content -->
<div id="import" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Import Uraian Singkat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" action="{{route('index_import',$buku->id_buku)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-body">
                    
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">File Excel</label>
                        <div class="col-9">
                            <input type="file" name="file" class="form-control" id="file">
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->