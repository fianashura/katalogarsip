@extends('home::layouts.master')

@section('title')
    Arsip | Khasanah
@endsection

@section('css')
  <!-- plugin css -->
  <link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />   
@endsection

@section('content')
<div class="content">
                    
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Khasanah</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">Khasanah</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-1">Khasanah
                        <button type="button" class="btn btn-primary btn-sm" onclick="addForm()"><i data-feather="plus"></i></button>
                    </h4>
                        <table id="tableKhasanah" class="table dt-responsive" style="width:100%">
                            <thead>
                                <tr class="text-center">
                                    <th style="width:10%">#</th>
                                    <th style="width:50%">Khasanah</th>
                                    <th style="width:20%">Arsip</th>
                                    <th style="width:5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @foreach ($buku as $bu)    
                                    <tr>
                                        <td>{{$no++}}.</td>
                                        <td><a href="{{route('index_buku',$bu->id_buku)}}"><b>{{$bu->judul_buku}}</b></a></td>
                                        <td>
                                            <button class="btn btn-warning btn-sm"><i data-feather="edit"></i></button>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-sm"><i data-feather="trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach --}}
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container-fluid -->
    @include('buku::form')
</div> <!-- content -->
@endsection

@section('js')

    <!-- datatable js -->
    <script src="{{asset('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>

    <!-- Datatables init -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        let table = $('#tableKhasanah').DataTable({
            processing: true,
            serverSide: true,
            ajax: `{{route('buku.data')}}`,
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable:false, searchable: false, className: "text-center" },
                { data: 'judul_buku', name: 'judul_buku' },
                { data: 'total_arsip', name: 'total_arsip', className: "text-center"},
                { data: 'action', name: 'action', orderable:false, searchable: false, className: "text-center" }
            ]
        });

        function addForm() {
          save_method = "add";
          $('input[name=_method]').val('POST');
          $('#myModal').modal('show');
          $('.modal-title').text('Tambah Buku');
          $('#myModal form')[0].reset();
        }
    
        function editForm(id) {
          save_method = 'edit';
          $('input[name=_method]').val('POST');
          $('#myModal form')[0].reset();
          $.ajax({
            url: "{{ url('buku') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
              $('#myModal').modal('show');
              $('.modal-title').text('Edit Khasanah');
    
              $('#id').val(data.id_buku);
              $('#judul_buku').val(data.judul_buku);
            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }

        function deleteData(id){
          var csrf_token = $('meta[name="csrf-token"]').attr('content');
          swal.fire({
              title: 'Apa anda yakin?',
              text: "Anda tidak dapat membatalkan perintah ini!",
              icon: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yakin!'
            }).then((result) => {
            if (result.isConfirmed) {
              $.ajax({
                  url : "{{ url('buku') }}" + '/' + id + '/hapus',
                  type : "POST",
                  data : {'_method' : 'POST', '_token' : '{{ csrf_token() }}'},
                  success : function(data) {
                      table.ajax.reload();
                      swal.fire({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                      })
                  },
                  error : function () {
                      swal.fire({
                          title: 'Oops...',
                          text: 'Gagal',
                          type: 'error',
                          timer: '1500'
                      })
                  }
              });
            };
          });
        }
        $(function(){
              $('#myModal').on('submit', function (e) {
                  if (!e.isDefaultPrevented()){
                      var id = $('#id').val();
                      if (save_method == 'add') url = `{{ route('buku.simpan') }}`;
                      else url = `{{url('buku')}}/${id}/update`;
    
                      $.ajax({
                          url : url,
                          type : "POST",
                          //data : $('#myModal form').serialize(),
                          data : new FormData($('#myModal form')[0]),
                          contentType : false,
                          processData : false,
                          success : function(data) {
                              $('#myModal').modal('hide');
                              table.ajax.reload();
                              swal.fire({
                                  title: data.title,
                                  text: data.message,
                                  icon: data.tipe,
                                  timer: '1500'
                              })
                          },
                          error : function(){
                              swal.fire({
                                  title: 'Oops...',
                                  text: data.message,
                                  type: 'error',
                                  timer: '1500'
                              })
                          }
                      });
                      return false;
                  }
              });
          });
    </script>
@endsection