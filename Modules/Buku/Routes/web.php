<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']],
function(){ 
    Route::prefix('buku')->group(function() {
        Route::get('/', 'BukuController@buku')->name('buku');
        Route::get('/data-buku', 'BukuController@dataBuku')->name('buku.data');
        Route::post('/simpan', 'BukuController@buku_simpan')->name('buku.simpan');
        Route::get('/{id}/edit', 'BukuController@edit_buku')->name('buku.edit');
        Route::post('/{id}/update', 'BukuController@update_buku')->name('buku.update');
        Route::post('/{id}/hapus', 'BukuController@hapus_buku')->name('buku.hapus');
        Route::get('/info', 'BukuController@info')->name('info');
        
        Route::get('/index/{id_buku}', 'BukuController@index')->name('index_buku');
        Route::post('/index/{id_buku}/simpan', 'BukuController@index_simpan')->name('index_simpan');
        Route::post('/index/{id_buku}/import', 'BukuController@index_import')->name('index_import');
    });
});
