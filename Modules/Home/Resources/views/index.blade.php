@extends('home::layouts.master')

@section('title')
    Arsip | Home
@endsection

@section('css')
    <!-- plugins -->
    <link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row page-title align-items-center">
            <div class="col-sm-4 col-xl-6">
                <h4 class="mb-1 mt-0">Dashboard</h4>
            </div>
        </div>

        <!-- content -->
        <div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total Khasanah</span>
                                <h2 class="mb-0">{{$khasanah}}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-revenue-chart" class="apex-charts"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total Arsip</span>
                                <h2 class="mb-0">{{$arsip}}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-product-sold-chart" class="apex-charts"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Pengunjung</span>
                                <h2 class="mb-0">{{$pengunjung}}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-new-customer-chart" class="apex-charts"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Pengunjung Kemarin</span>
                                <h2 class="mb-0">{{$pengunjung_kemarin}}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-new-visitors-chart" class="apex-charts"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row -->

    </div>
</div> <!-- content -->
@endsection

@section('js')
        <!-- optional plugins -->
        <script src="{{asset('assets/libs/moment/moment.min.js')}}"></script>
        <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>
        <script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>

        <!-- page js -->
        <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
@endsection