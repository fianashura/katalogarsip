<?php

namespace Modules\Home\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if(Auth::user()->role == 'User'){
            return redirect('/');
        }
        $date = date('Y-m-d');
        $kemarin = date('Y-m-d', strtotime($date. ' - 1 days'));
        $pengunjung = DB::table('tbl_pengunjung')
            ->where('tgl_dtng',$date)
            ->count();
        $pengunjung_kemarin = DB::table('tbl_pengunjung')
            ->where('tgl_dtng',$kemarin)
            ->count();
        $khasanah = DB::table('tbl_buku')
            ->count();
        $arsip = DB::table('tbl_index')
            ->count();
        return view('home::index', compact('pengunjung','pengunjung_kemarin', 'khasanah','arsip'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('home::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('home::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('home::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
