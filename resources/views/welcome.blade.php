@extends('layouts.app')

@section('title')
    Arsip | Home
@endsection

@section('css')
    <!-- plugins -->
    <link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row page-title align-items-center">
            <div class="col-sm-4 col-xl-6">
                {{-- <h4 class="mb-1 mt-0">Dashboard</h4> --}}
            </div>
        </div>

        <!-- content -->
        <div class="row">
            <div class="col-md-12">
                <img src="{{asset('assets/images/website.png')}}" alt="" style="width: 100%">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <a href="{{route('indexBuku')}}">
                            <div class="media p-3">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold"><br></span>
                                    <h2 class="mb-0">Khasanah</h2>
                                </div>
                                <div class="align-self-center">
                                    <h1><i class='uil uil-books'></i></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <a href="#">
                            <div class="media p-3">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Comming Soon ...</span>
                                    <h2 class="mb-0">Baca</h2>
                                </div>
                                <div class="align-self-center">
                                    <h1><i class='uil uil-book-open'></i></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <a href="#">
                            <div class="media p-3">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Comming Soon ...</span>
                                    <h2 class="mb-0">Akun</h2>
                                </div>
                                <div class="align-self-center">
                                    <h1><i class='uil uil-user-square'></i></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            {{-- <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">New
                                    Visitors</span>
                                <h2 class="mb-0">750</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-new-visitors-chart" class="apex-charts"></div>
                                <span class="text-danger font-weight-bold font-size-13"><i
                                        class='uil uil-arrow-down'></i> 5.05%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
        <!-- row -->

    </div>
</div> <!-- content -->
@endsection

@section('js')
        <!-- optional plugins -->
        <script src="{{asset('assets/libs/moment/moment.min.js')}}"></script>
        <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>
        <script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>

        <!-- page js -->
        <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
@endsection