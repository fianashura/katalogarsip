@extends('layouts.app')

@section('title')
    Arsip | Khasanah
@endsection

@section('css')
  <!-- plugin css -->
  <link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />   
@endsection

@section('content')
<div class="content">
                    
    <!-- Start Content-->
    <!--<div class="container-fluid d-none d-sm-block d-md-none">-->
    <!--    <div class="row page-title">-->
    <!--        <div class="col-md-12">-->
    <!--            <nav aria-label="breadcrumb" class="float-right mt-1">-->
    <!--                <ol class="breadcrumb">-->
    <!--                    <li class="breadcrumb-item"><a href="#">Home</a></li>-->
    <!--                    <li class="breadcrumb-item active" aria-current="page">Khasanah</li>-->
    <!--                </ol>-->
    <!--            </nav>-->
    <!--            <h4 class="mb-1 mt-0">Khasanah</h4>-->
    <!--        </div>-->
    <!--    </div>-->

    <!--    <div class="row">-->
    <!--        <div class="col-12">-->
    <!--            <div class="card">-->
    <!--                <div class="card-body">-->
    <!--                    <h4 class="header-title mt-0 mb-1">Khasanah-->
    <!--                </h4>-->
    <!--                    <table id="tableKhasanah" class="table dt-responsive" style="width:100%">-->
    <!--                        <thead>-->
    <!--                            <tr class="text-center">-->
    <!--                                <th style="width:10%">#</th>-->
    <!--                                <th style="width:80%">Khasanah</th>-->
    <!--                                <th style="width:5%"></th>-->
    <!--                                {{-- <th style="width:5%"></th> --}}-->
    <!--                            </tr>-->
    <!--                        </thead>-->
    <!--                        <tbody>-->
    <!--                            {{-- @foreach ($buku as $bu)    -->
    <!--                                <tr>-->
    <!--                                    <td>{{$no++}}.</td>-->
    <!--                                    <td><a href="{{route('index_buku',$bu->id_buku)}}"><b>{{$bu->judul_buku}}</b></a></td>-->
    <!--                                    <td>-->
    <!--                                        <button class="btn btn-warning btn-sm"><i data-feather="edit"></i></button>-->
    <!--                                    </td>-->
    <!--                                    <td>-->
    <!--                                        <button class="btn btn-danger btn-sm"><i data-feather="trash"></i></button>-->
    <!--                                    </td>-->
    <!--                                </tr>-->
    <!--                            @endforeach --}}-->
    <!--                        </tbody>-->
    <!--                    </table>-->

    <!--                </div> <!-- end card body-->
    <!--            </div> <!-- end card -->
    <!--        </div><!-- end col-->
    <!--    </div>-->
        <!-- end row-->

    <!--</div> <!-- container-fluid -->
    
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <h4 class="mb-1 mt-0">Daftar Khasanah</h4>
            </div>
        </div>

        @foreach ($buku as $item)
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{route('indexArsip',$item->id_buku)}}"><b>{{$item->judul_buku}}</b></a>
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        @endforeach
        <!-- end row-->

    </div> <!-- container-fluid -->
</div> <!-- content -->
@endsection

@section('js')

    <!-- datatable js -->
    <script src="{{asset('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>

    <!-- Datatables init -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        let table = $('#tableKhasanah').DataTable({
            processing: true,
            serverSide: true,
            ajax: `{{route('dataBuku')}}`,
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable:false, searchable: false, className: "text-center" },
                { data: 'judul_buku', name: 'judul_buku' },
                // { data: 'action', name: 'action', orderable:false, searchable: false, className: "text-center" }
            ]
        });
    </script>
@endsection