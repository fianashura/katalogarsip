
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from shreyu.coderthemes.com/pages-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Jul 2020 22:53:26 GMT -->
<head>
        <meta charset="utf-8" />
        <title>Registrasi | Arsip DPK Sulsel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    </head>

    <body class="authentication-bg">
        
        <div class="account-pages my-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-lg-6 p-5">
                                        <div class="mx-auto mb-5">
                                            <a href="index.html">
                                                <img src="{{asset('assets/images/logo.png')}}" alt="" height="24" />
                                                <h3 class="d-inline align-middle ml-1 text-logo">Arsip DPK Sulsel</h3>
                                            </a>
                                        </div>

                                        <h6 class="h5 mb-0 mt-4">Buat Akun</h6><br>
                                        {{-- <p class="text-muted mt-0 mb-4">Create a free account and start using Shreyu</p> --}}

                                        @if (\Session::has('success'))
                                            <div class="alert alert-success">
                                                {!! \Session::get('success') !!}
                                            </div>
                                        @endif
                                        <form action="{{route('registrasi')}}" class="authentication-form" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label class="form-control-label">Nama Anda</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="user"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Your full name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-control-label">E-Mail Anda</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="mail"></i>
                                                        </span>
                                                    </div>
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="hello@gmail.com">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="form-control-label">Password</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="lock"></i>
                                                        </span>
                                                    </div>
                                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-control-label">No HP Anda</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="phone"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Your Phone Number">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="form-control-label">Alamat Anda</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="map"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Your Address">
                                                </div>
                                            </div>
                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-primary btn-block" type="submit">Daftar</button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                    <div class="col-lg-6 d-none d-md-inline-block">
                                        <div class="auth-page-sidebar">
                                            <div class="overlay"></div>
                                            <div class="auth-user-testimonial">
                                                <p class="font-size-24 font-weight-bold text-white mb-1">I simply love it!</p>
                                                <p class="lead">"It's a elegent templete. I love it very much!"
                                                </p>
                                                <p>- Admin User</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p class="text-muted">Sudah Punya Akun? <a href="{{route('login')}}" class="text-primary font-weight-bold ml-1">Login</a></p>
                            </div> <!-- end col -->
                            <div class="col-12 text-center">
                                <p class="text-muted"><a href="{{route('/')}}" class="text-primary font-weight-bold ml-1"><i class="icon-dual" data-feather="arrow-left"></i> Halaman Utama</a></p>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="{{asset('assets/js/vendor.min.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('assets/js/app.min.js')}}"></script>
        
    </body>

<!-- Mirrored from shreyu.coderthemes.com/pages-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Jul 2020 22:53:26 GMT -->
</html>
