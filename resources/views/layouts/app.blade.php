
<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

        @yield('css')

        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body>
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            <div class="navbar navbar-expand flex-column flex-md-row navbar-custom">
                <div class="container-fluid">
                    <!-- LOGO -->
                    <a href="index.html" class="navbar-brand mr-0 mr-md-2 logo">
                        <span class="logo-lg">
                            <img src="{{asset('assets/images/logo.png')}}" alt="" height="24" />
                            <span class="d-inline h5 ml-1 text-logo">Arsip</span>
                        </span>
                        <span class="logo-sm">
                            <img src="{{asset('assets/images/logo.png')}}" alt="" height="24">
                        </span>
                    </a>

                    <ul class="navbar-nav bd-navbar-nav flex-row list-unstyled menu-left mb-0">
                        <li class="">
                            <button class="button-menu-mobile open-left disable-btn">
                                <i data-feather="menu" class="menu-icon"></i>
                                <i data-feather="x" class="close-icon"></i>
                            </button>
                        </li>
                    </ul>

                    <ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="login">
                            <a href="{{ route('login') }}"  class="nav-link right-bar-toggle">
                                <i data-feather="log-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu d-xs-none">
                <div class="media user-profile mt-2 mb-2">
                    <img src="{{asset('assets/images/users/avatar-7.jpg')}}" class="avatar-sm rounded-circle mr-2" alt="Shreyu" />
                    <img src="{{asset('assets/images/users/avatar-7.jpg')}}" class="avatar-xs rounded-circle mr-2" alt="Shreyu" />

                    <div class="media-body">
                        @if (Auth::user())
                        <h6 class="pro-user-name mt-0 mb-0">User</h6>
                        <span class="pro-user-desc">{{Auth::user()->name}}</span>
                        @else
                        <h6 class="pro-user-name mt-0 mb-0">Guest</h6>
                        <span class="pro-user-desc">Guest</span>
                        @endif
                    </div>
                    <div class="dropdown align-self-center profile-dropdown-menu">
                        <a class="dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                            aria-expanded="false">
                            <span data-feather="chevron-down"></span>
                        </a>
                        <div class="dropdown-menu profile-dropdown">
                            <a href="pages-profile.html" class="dropdown-item notify-item">
                                <i data-feather="user" class="icon-dual icon-xs mr-2"></i>
                                <span>My Account</span>
                            </a>

                            <div class="dropdown-divider"></div>

                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-close="true" class="dropdown-item notify-item" >
                                <i data-feather="log-out" class="icon-dual icon-xs mr-2"></i>
                                <span>Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="sidebar-content">
                    <!--- Sidemenu -->
                    <div id="sidebar-menu" class="slimscroll-menu">
                        <ul class="metismenu" id="menu-bar">

                            <li>
                                <a href="{{route('/')}}">
                                    <i data-feather="home"></i>
                                    <span> Home </span>
                                </a>
                            </li>
                            {{-- <li class="menu-title">Rak</li>
                            <li>
                                <a href="{{route('rak')}}">
                                    <i data-feather="layers"></i>
                                    <span> Data Rak </span>
                                </a>
                            </li> --}}
                            <li class="menu-title">Inventaris</li>
                            <li>
                                <a href="{{route('indexBuku')}}">
                                    <i data-feather="book"></i>
                                    <span> Khasanah </span>
                                </a>
                            </li>
                            
                            
                        </ul>
                    </div>
                    <!-- End Sidebar -->
                    <!-- Bottom Navbar -->
                    <div class="clearfix"></div>
                </div>
                <!-- Sidebar -left -->
                
            </div>
            <!-- Left Sidebar End -->
            

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                @yield('content')

                

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                2022 &copy; Shreyu. All Rights Reserved. <a href="#">Dinas Perpustakaan & Kearsipan Provinsi Sulawesi Selatan</a>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->


        <!-- Vendor js -->
        <script src="{{asset('assets/js/vendor.min.js')}}"></script>

        @yield('js')

        <!-- App js -->
        <script src="{{asset('assets/js/app.min.js')}}"></script>


    </body>

</html>