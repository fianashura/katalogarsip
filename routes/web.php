<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'webController@index')->name('/');

Route::get('/web/buku', 'webController@indexBuku')->name('indexBuku');
Route::get('/web/buku/data', 'webController@dataBuku')->name('dataBuku');

Route::get('/web/index/{id_buku}', 'webController@indexArsip')->name('indexArsip');

Route::post('/registrasi', 'webController@createUser')->name('registrasi');

Auth::routes();

// Route::get('/phpinfo', 'BukuController@info')->name('info');
